# DockerizingAngularDemo
## Dockerfile
```
FROM node:12.2.0 as build-stage 
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get update && apt-get install -yq google-chrome-stable 
WORKDIR /app
COPY . /app
RUN npm install
CMD [ "npm","run","test" ]
# (or)
CMD [ "npm","run","e2e" ]
``` 

## .dockerignore  

```
node_modules
.git
.gitignore
package-lock.json
dist
```  

## Karma.config.js  

```
browsers: ['ChromeHeadless'],
    customLaunchers: {
      'ChromeHeadless': {
        base: 'Chrome',
        flags: [
          '--no-sandbox',
          '--headless',
          '--disable-gpu',
          '--remote-debugging-port=9222'
        ]
      }
    }
```  

## protractor.config.js  

```
capabilities: {
    browserName: 'chrome',
    'chromeOptions': {
      'args': [
        '--no-sandbox',
        '--headless',
        '--window-size=1024,768'
      ]
    }
  }
```  

## For Running unit testing

```
ng test --watch=false
```
## For Running e2e testing

```
ng e2e
```  

## Docker commands  

### For Building image from the Dockerfile

```
docker build -t angular-testing .
```  

**angular-testing**-->Image name  
**Period (.)**--> Contents of the current directory  

### For creating the container

```
docker run --name angular-testing-container angular-testing
``` 
**angular-testing-container**-->container name  
**angular-testing**-->Image name   

### For creating the container with access to its bash/terminal  

```
docker run --name angular-testing-container -it  angular-testing /bin/bash
```

### For Removing the image

```
docker rmi angular-testing
```

### For Removing the container 

```
docker rm angular-testing-container
```  

### For stop/start the container

```
docker start/stop angular-testing-container
```  

### For listing out all the containers  

```
docker ps -a
```  

### For listing out the Running containers  

```
docker ps
```  

### For listing out all the images  

```
docker images
```

